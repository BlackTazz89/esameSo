#define _POSIX_C_SOURCE 200809L
#define BUFSIZE 10
#define N_MAX_CLIENTS 10

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <stdbool.h>
#include <sys/mman.h>

int parse_int(char* buf, int bufsize, FILE* fp){
    if(fgets(buf, bufsize, fp) == NULL){
        return -1;
    }
    int res = 0;
    errno = 0;
    res = strtol(buf, NULL, 10);
    if(errno == ERANGE){
        return -1;
    }
    return res;
}

int write_int(int value, FILE* fp){
    if(fprintf(fp, "%d\n", value) < 0){
        return -1;
    }
    if(fflush(fp) == EOF){
        return -1;
    }
}

int main(int argc, char** argv){
    sleep(1);
    time_t t;
    if(time(&t) == ((time_t) -1)){
        perror("error while getting time from epoch");
        exit(EXIT_FAILURE);
    }
    srand((unsigned) t);

    sem_t* client_sem;
    sem_t* pizza_maker_sem;
    sem_t* mutex;
    int res = EXIT_SUCCESS;
    const char* client_sem_name = "/es1_client_sem";
    const char* pizza_maker_sem_name = "/es1_pizza_maker_sem";
    const char* mutex_name="/es1_mutex";
    const char* fifo_name = "/tmp/es1_fifo";
    const char* shm_mem_name = "/es1_shm";
    int* n_clients;
    if((client_sem = sem_open(client_sem_name, O_RDWR)) == SEM_FAILED){
        perror("error while opening semaphore");
        exit(EXIT_FAILURE);
    }
    if((pizza_maker_sem = sem_open(pizza_maker_sem_name, O_RDWR)) == SEM_FAILED){
        perror("error while opening semaphore");
        res = EXIT_FAILURE;
        goto err1;
    }
    FILE* fifo_fp;
    if((fifo_fp = fopen(fifo_name, "r+")) == NULL){
        perror("error while opening fifo");
        res = EXIT_FAILURE;
        goto err2;
    }
    int fd;
    if((fd = shm_open(shm_mem_name, O_RDWR, S_IRUSR | S_IWUSR)) == -1){
        perror("error while opening shared mem");
        res = EXIT_FAILURE;
        goto err3;
    }
    if((n_clients = mmap(NULL, sizeof(int)*2, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == ((void*)-1)){
        perror("error while mapping memory");
        res = EXIT_FAILURE;
        goto err4;
    }
    if((mutex = sem_open(mutex_name, O_RDWR)) == SEM_FAILED){
        perror("error while opening mutex");
        res = EXIT_FAILURE;
        goto err5;
    }
    int user_id=0;
    sem_wait(mutex);
    ++(*(n_clients+1));
    if(*n_clients >= N_MAX_CLIENTS){
        printf("Cliente %d: c'è troppa fila... tornerò un altro giorno.\n", *(n_clients+1));
        sem_post(mutex);
        exit(EXIT_SUCCESS);
    }
    if(*n_clients > 0){
        printf("Cliente %d: Ah, %d person%c davanti a me, aspetterò\n", *(n_clients+1), *n_clients, *n_clients > 1 ? 'e' : 'a');
    }
    user_id=*(n_clients+1);
    ++(*n_clients);
    sem_post(mutex);
    sleep(rand() % 3);
    sem_wait(mutex);
    sem_post(pizza_maker_sem);
    sem_wait(client_sem);
    char buf[BUFSIZE];
    int pizza_slices = rand() % 11;
    printf("Cliente %d: Buongiorno, vorrei %d tranci di pizza\n", user_id, pizza_slices);
    if(write_int(pizza_slices, fifo_fp) == -1){
        perror("error while asking for pizza slices");
        res = EXIT_FAILURE;
        goto err6;
    }
    sem_post(pizza_maker_sem);
    sem_wait(client_sem);
    int slice_price = 0;
    int order_price = 0;
    if((slice_price = parse_int(buf,BUFSIZE, fifo_fp)) == -1){
        perror("error while getting slice price");
        res = EXIT_FAILURE;
        goto err6;
    }
    if((order_price = parse_int(buf,BUFSIZE, fifo_fp)) == -1){
        perror("error while getting order price");
        res = EXIT_FAILURE;
        goto err6;
    }
    printf("Cliente %d: Ah... quindi ogni trancio costa %d euro e le devo dare in totale %d euro\n", user_id, slice_price, order_price);
    bool answer = rand() & 1;
    printf("Cliente %d: ", user_id);
    puts(answer ? "Accetto" : "Mi pare irragionevole... arrivederci");
    if(write_int(answer, fifo_fp) == -1){
        perror("error while choosing if accept the pizza maker price");
        res = EXIT_FAILURE;
        goto err6;
    }
    sem_post(pizza_maker_sem);
    sleep(2);

err6:
    if(sem_close(mutex) == -1){
        perror("error while closing mutex");
        res = EXIT_FAILURE;
    }
err5:
    if(munmap(n_clients, 1) == -1){
        perror("error while unmapping shared memory");
        res = EXIT_FAILURE;
    }
err4:
    if(close(fd) == -1){
        perror("error while closing shared memory");
        res = EXIT_FAILURE;
    }
err3:
    if(fclose(fifo_fp) == EOF){
        perror("error while closing fifo");
        res = EXIT_FAILURE;
    }
err2:
    if(sem_close(pizza_maker_sem) == -1){
        perror("error while closing semaphore");
        res = EXIT_FAILURE;
    }
err1:
    if(sem_close(client_sem) == -1){
        perror("error while closing semaphore");
        res = EXIT_FAILURE;
    }

return res;

}
