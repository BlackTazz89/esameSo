#define _POSIX_C_SOURCE 200809L
#define DEFAULT_SENTENCES_N 1
#define LETTERS_N 52
#define swap(t, x, y) t temp = x; x = y; y = temp;

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <sys/stat.h>

float total_chars;
int occurrences[LETTERS_N];

pthread_mutex_t mutex;

void get_sentences(char* line, ssize_t linesize, char*** sentences, int* n_sentences, int* sentences_size){
    char** ptr;
    char** eptr;
    if(*sentences == NULL || *sentences_size == 0){
        if((*sentences = malloc(DEFAULT_SENTENCES_N*sizeof(char*))) == NULL && errno == ENOMEM){
            perror("error while allocating memory");
            exit(EXIT_FAILURE);
        }
        *sentences_size = DEFAULT_SENTENCES_N;
        *n_sentences = 0;
    }
    ptr = *sentences + *n_sentences;
    eptr = *sentences + *sentences_size;

    int start = 0;
    for(int i=0; i<linesize; i++){
        if(line[i] == '.'){
            int size = i - start;
            char* dst = NULL;
            if((dst = malloc(size + 1)) == NULL && errno == ENOMEM){
                perror("error while allocating memory");
                exit(EXIT_FAILURE);
            }
            *ptr++ = strncpy(dst, line+start, size);
            *(dst+size) = '\0';
            start += size + 1;
            ++*n_sentences;
        }
        if(ptr == eptr){
            char** n_buf;
            int n_size = *sentences_size*2;
            if((n_buf = realloc(*sentences, n_size*sizeof(char*))) == NULL && errno == ENOMEM){
                perror("error while reallocating memory");
                exit(EXIT_FAILURE);
            }
            *sentences = n_buf;
            ptr = n_buf + *sentences_size;
            eptr = n_buf + n_size;
            *sentences_size = n_size;
        }
    }
}

void* increment(void* arg){
    char c = *((char*) arg);
    pthread_mutex_lock(&mutex);
    if(c>0x40 && c<0x5b){
        ++occurrences[c - 0x41];
        ++total_chars;
    } else if(c>0x60 && c<0x7b){
        ++occurrences[c - 0x47];
        ++total_chars;
    } else {
        printf("Ignoro il carattere '%c'. Non è un carattere dell'alfabeto\n", c);
    }
    pthread_mutex_unlock(&mutex);
    return NULL;

}

void frequenza(char* str){
    printf("\nSto analizzando la frase -> %s\n", str);
    size_t len = strlen(str);
    pthread_t threads[len];
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    for(int i = 0; i < len; i++){
        pthread_create(&threads[i], &attr, &increment, &str[i]);
    }
    pthread_attr_destroy(&attr);
    for(int i = 0; i < len; i++){
        pthread_join(threads[i], NULL);
    }
}

void sort(int arr[], int begin, int end) {
    float pivot;
    int left;
    int right;
    if(end > begin){
        pivot = occurrences[arr[begin]]/total_chars;
        left = begin + 1;
        right = end + 1;
        while(left < right){
            if(occurrences[arr[left]]/total_chars < pivot){
                ++left;
            } else {
                --right;
                swap(int, arr[left], arr[right]);
            }
        }
        --left;
        swap(int, arr[begin], arr[left]);
        sort(arr, begin, left - 1);
        sort(arr, left + 1, end);
    }
}

int main(int argc, char** argv){

    char** sentences = NULL;
    int n_sentences = 0;
    int sentences_size = 0;
    FILE* fp;
    if((fp = fopen("huffman.txt", "r")) == NULL){
        perror("error opening file");
        exit(EXIT_FAILURE);
    }
    char* line = NULL;
    size_t n = 0;
    ssize_t linesize = 0;
    while((linesize = getline(&line, &n, fp)) != -1){
        get_sentences(line, linesize, &sentences, &n_sentences, &sentences_size);
        free(line);
        line=NULL;
    }
    
    pthread_mutexattr_t mutex_attr;
    pthread_mutexattr_init(&mutex_attr);
    pthread_mutex_init(&mutex, &mutex_attr);
    pthread_mutexattr_destroy(&mutex_attr);

    for(int i=0; i<n_sentences; i++){
        frequenza(*(sentences + i));
    }
    
    printf("\nIl numero totale dei caratteri è: %f\n\n", total_chars);
    
    int chars[LETTERS_N];
    for(int i = 0; i < LETTERS_N; i++){
        chars[i] = i;
    }
    sort(chars, 0, LETTERS_N - 1);
    for(int i = 0; i < LETTERS_N; i++){
        char c = chars[i];
        if(c>=0 && c<26){
            c += 0x41;
        } else if(c>=26 && c<52){
            c += 0x47;
        }
        printf("Carattere %c -> %.2f%%\n", c, occurrences[chars[i]]/total_chars);
    }

    fclose(fp);
    exit(EXIT_SUCCESS);
}
