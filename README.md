
1) In una pizzeria al taglio c'è un pizzaiolo che,  se non ci sono clienti, aspetta leggendo un
giornale.  Il cliente appena arriva, chiama il pizzaiolo comunicandogli la quantità di pizza 
desiderata. A questo punto si instaura una piccolo protocollo di interazione cliente-
pizzaiolo.
Il pizzaiolo si alza, legge il numero di tagli richiesti e comunica al cliente il prezzo di un 
solo taglio di pizza e del totale dei tagli richiesti, chiedendo al cliente se accetta il prezzo. Il
cliente risponde. Se il cliente accetta, il pizzaiolo serve la quantità di pizza richiesta e 
riprende a leggere. Se il cliente rifiuta, il pizzaiolo riprende subito a leggere. Descrivere il 
pizzaiolo ed i clienti con due processi concorrenti indipendenti sincronizzati con dei 
semafori. Realizzare la comunicazione tra il cliente e il pizzaiolo con una Fifo: non appena 
il cliente arriva, alza il suo semaforo e scrive sulla fifo il numero di tagli. Non appena il 
pizzaiolo si sveglia, legge dalla fifo il numero e scrive sulla fifo il prezzo. Il cliente legge il 
prezzo e scrive si o no, e così via.
Realizzare una versione nella quale le domande e le risposte sono generate casualmente.
Il ciclo di vita del cliente e del pizzaiolo vengono tracciati sullo standard output. Stabilire un
numero massimo di clienti, per esempio 5 o 10.

2) Realizzare un server che gestisce un Conto Corrente CC. Il CC viene inizializzato e 
comunque modificato saltuariamente dal processo Banca. Il CC viene utilizzato da un 
certo numero di utenti concorrenti, che possono prelevare o depositare determinate 
somme sul CC. Gli utenti usano un sistema di messaggistica realizzato con una memoria 
condivisa tra server ed utenti. Quando un utente vuole depositare una somma sul CC, 
manda un messaggio contenente una cifra positiva, quando vuole prelevare manda un 
messaggio con una cifra negativa. Naturalmente gli utenti concorrenti operano in mutua 
esclusione. Inoltre operano in mutua esclusione col processo saltuario Banca. Viceversa, 
quando il processo Banca opera, sono bloccati tutti gli utenti.

3) Una componente fondamentale dell'algoritmo di compressione testi di Huffmann è 
quella che  calcola la frequenza delle lettere di un testo.
Scrivere un programma che legge un file di testo riga per riga. S
crivere una funzione che,
ogni volta che viene chiamata, 
estrae da ogni riga tutti i caratteri circondati da punti, 
cioè le frasi del testo. Quando si arriva alla fine della riga ritorna il carattere terminatore di stringa. 
Le frasi, cioè I caratteri circondati dai punti, vengono caricate in un array 
 bidimensionale di 
stringhe. Le stringhe vengono  poi lette ed analizzate per estrarre la frequenza delle lettere dalla 
funzione “frequenza(str)” che crea un thread per ogni lettera della stringa. La funzione dei thread è 
di incrementare un contatore, uno per ogni lettera dell'alfabeto. Quando tutti I thread sono terminati
per tutte  le frasi del testo, I contatori contengono la numerosità dei caratteri che, divisi per il 
numero dei caratteri, forniscono la frequenza dei caratteri. Scrivere allora una funzione che ordina I
caratteri per frequenza e che scrive I risultati sullo standard output.
L'algoritmo di Huffman verrà completato prossimamente.

4) Realizzare una shell in Bash, cioè realizzare una shell con una shell.
Realizzare cioè l'algoritmo
while(true) {
scrivi il prompt %%
leggi la linea scritta dall'utente;
fai il parsing della linea;
per ogni token ottenuto
se il token è un simbolo di pipe, realizza una pipe
se il token è un simbolo di redirezione, effettua la redirezione
se il token è un comando, chiama il comando
}
