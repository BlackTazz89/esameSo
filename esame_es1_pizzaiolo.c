#define _POSIX_C_SOURCE 200809L
#define BUFSIZE 10

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <sys/mman.h>
#include <signal.h>

int parse_int(char* buf, int bufsize, FILE* fp){
    if(fgets(buf, bufsize, fp) == NULL){
        return -1;
    }
    int res = 0;
    errno = 0;
    res = strtol(buf, NULL, 10);
    if(errno == ERANGE){
        return -1;
    }
    return res;
}

int write_int(int value, FILE* fp){
    if(fprintf(fp, "%d\n", value) < 0){
        return -1;
    }
    if(fflush(fp) == EOF){
        return -1;
    }
}

void clean_close(int signum){
    puts("Exit cleanly");
    int res = EXIT_SUCCESS;
    if(sem_unlink("/es1_mutex") == -1){
        perror("error while unlinking mutex");
        res = EXIT_FAILURE;
    }
    if(shm_unlink("/es1_shm") == -1){
        perror("error while unlinking shared memory");
        res = EXIT_FAILURE;
    }
    if(unlink("/tmp/es1_fifo") == -1){
        perror("error while unlinking fifo");
        res = EXIT_FAILURE;
    }
    if(sem_unlink("/es1_pizza_maker_sem") == -1){
        perror("error while unlinking semaphore");
        res = EXIT_FAILURE;
    }
    if(sem_unlink("/es1_client_sem") == -1){
        perror("error while unlinking semaphore");
        res = EXIT_FAILURE;
    }
    exit(res);
}

int main(int argc, char** argv){
    time_t t;
    if(time(&t) == ((time_t) -1)){
        perror("error while getting time from epoch");
        exit(1);
    }
    srand((unsigned) t);

    struct sigaction on_close;
    on_close.sa_handler = clean_close;
    sigemptyset(&on_close.sa_mask);
    on_close.sa_flags = 0;
    if(sigaction(SIGINT, &on_close, NULL) == -1){
        perror("error while adding a SIGINT action");
        exit(EXIT_FAILURE);
    }
    if(sigaction(SIGTERM, &on_close, NULL) == -1){
        perror("error while adding a SIGINT action");
        exit(EXIT_FAILURE);
    }

    int res = EXIT_SUCCESS;
    sem_t* client_sem;
    sem_t* pizza_maker_sem;
    sem_t* mutex;
    const char* client_sem_name="/es1_client_sem";
    const char* pizza_maker_sem_name="/es1_pizza_maker_sem";
    const char* mutex_name="/es1_mutex";
    const char* fifo_name="/tmp/es1_fifo";
    const char* shm_mem_name = "/es1_shm";
    int* n_clients;
    if((client_sem = sem_open(client_sem_name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR, 0)) == SEM_FAILED){
        perror("error during semaphore creation");
        exit(1);
    }
    if((pizza_maker_sem = sem_open(pizza_maker_sem_name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR, 0)) == SEM_FAILED){
        perror("error during semaphore creation");
        res = EXIT_FAILURE;
        goto err1;
    }
    if(mkfifo(fifo_name, S_IRUSR | S_IWUSR) == -1){
        perror("error during creation of fifo");
        res = EXIT_FAILURE;
        goto err2;
    };
    FILE* fifo_fp;
    if((fifo_fp = fopen(fifo_name, "r+")) == NULL){
        perror("error while opening fifo");
        res = EXIT_FAILURE;
        goto err3;
    }
    int fd;
    if((fd = shm_open(shm_mem_name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR)) == -1){
        perror("error while opening shared mem");
        res = EXIT_FAILURE;
        goto err4;
    }
    if(ftruncate(fd, sizeof(int)*2) == -1){
        perror("error while initializatin shared memory");
        res = EXIT_FAILURE;
        goto err5;
    }
    if((n_clients = mmap(NULL, sizeof(int)*2, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == ((void*) -1)){
        perror("error while mapping memory");
        res = EXIT_FAILURE;
        goto err5;
    }
    if((mutex = sem_open(mutex_name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR, 1)) == SEM_FAILED){
        perror("error during mutex creation");
        res = EXIT_FAILURE;
        goto err6;
    }
    while(1){
        puts("Pizzaiolo: Gli articoli di oggi sono molto interessanti...");
        sem_wait(pizza_maker_sem);
        puts("Pizzaiolo: *oh! un cliente!*");
        char buf[BUFSIZE];
        puts("Pizzaiolo: Buongiorno, pizzeria da Nino, quanti tranci desidera?");
        sem_post(client_sem);
        sem_wait(pizza_maker_sem);
        int pizza_slices = 0;
        if((pizza_slices = parse_int(buf, BUFSIZE, fifo_fp)) == -1){
            perror("Error while getting client order");
            res = EXIT_FAILURE;
            goto err7;
        }
        int slice_price = rand() % 100;
        int order_price = pizza_slices * slice_price;
        printf("Pizzaiolo: Perfetto. Il costo di ogni trancio viene %d euro, il totale è quindi di %d euro. Le può andare bene?\n", slice_price, order_price);
        if(write_int(slice_price, fifo_fp) == -1 || write_int(order_price, fifo_fp) == -1){
            perror("error while talking with client about price");
            res = EXIT_FAILURE;
            goto err7;
        }
        sem_post(client_sem);
        sem_wait(pizza_maker_sem);
        int answer = 0;
        if((answer = parse_int(buf, BUFSIZE, fifo_fp)) == -1){
            perror("Error while getting client order");
            res = EXIT_FAILURE;
            goto err7;
        }
        if(answer){
            puts("Pizzaiolo: Ecco a lei i suoi tranci. Buona giornata!");
        } else {
            puts("Pizzaiolo: Arrivederci! *ok... tornerò a leggere*");
        }
        --(*n_clients);
        sem_post(mutex);
    }

err7:
    if(sem_close(mutex) == -1){
        perror("error while closing mutex");
        res = EXIT_FAILURE;
    }
    if(sem_unlink(mutex_name) == -1){
        perror("error while unlinking mutex");
        res = EXIT_FAILURE;
    }
err6:
    if(munmap(n_clients, 1) == -1){
        perror("error while unmapping shared memory");
        res = EXIT_FAILURE;
    }
err5:
    if(close(fd) == -1){
        perror("error while closing shared memory");
        res = EXIT_FAILURE;
    }
    if(shm_unlink(shm_mem_name) == -1){
        perror("error while unlinking shared memory");
        res = EXIT_FAILURE;
    }
err4:
    if(fclose(fifo_fp) == EOF){
        perror("error while closing fifo");
        res = EXIT_FAILURE;
    }
err3:
    if(unlink(fifo_name) == -1){
        perror("error while unlinking fifo");
        res = EXIT_FAILURE;
    }
err2:
    if(sem_close(pizza_maker_sem) == -1){
        perror("error while closing semaphore");
        res = EXIT_FAILURE;
    }
    if(sem_unlink(pizza_maker_sem_name) == -1){
        perror("error while unlinking semaphore");
        res = EXIT_FAILURE;
    }
err1:
    if(sem_close(client_sem) == -1){
        perror("error while closing semaphore");
        res = EXIT_FAILURE;
    }
    if(sem_unlink(client_sem_name) == -1){
        perror("error while unlinking semaphore");
        res = EXIT_FAILURE;
    }
    
return res;
    
}
