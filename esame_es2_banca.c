#define _POSIX_C_SOURCE 200809L
#define BUFSIZE sizeof(int)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>

void cleanup(int* buffer){
    if(shm_unlink("/es2_buffer") == -1 && errno != ENOENT){
        perror("error while removing shm buffer");
        exit(1);
    }
    if(sem_unlink("/es2_cc") == -1 && errno != ENOENT){
        perror("error while removing cc semaphore");
        exit(1);
    }
    if(munmap(buffer, BUFSIZE) == -1){
        perror("error while unmapping memory");
        exit(1);
    }
}

void clean_close(int signum){
    puts("Exit cleanly");
    int res = EXIT_SUCCESS;
    if(shm_unlink("/es2_buffer") == -1 && errno != ENOENT){
        perror("error while removing shm buffer");
        res = EXIT_FAILURE;
    }
    if(sem_unlink("/es2_cc") == -1 && errno != ENOENT){
        perror("error while removing cc semaphore");
        res = EXIT_FAILURE;
    }
    exit(res);
}

int main(int argc, char** argv){
    srand((unsigned) time(NULL));
    sem_t* cc;
    int fd;
    int* buffer;
    struct sigaction on_close;
    on_close.sa_handler = clean_close;
    sigemptyset(&on_close.sa_mask);
    on_close.sa_flags = 0;
    if(sigaction(SIGINT, &on_close, NULL) == -1){
        perror("error while adding a SIGINT action");
        exit(EXIT_FAILURE);
    }
    if(sigaction(SIGTERM, &on_close, NULL) == -1){
        perror("error while adding a SIGINT action");
        exit(EXIT_FAILURE);
    }
    if((cc = sem_open("/es2_cc", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR, 1)) == SEM_FAILED){
        perror("error during cc semaphore creation");
        exit(1);
    }
    if((fd = shm_open("/es2_buffer", O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR)) != -1){
        if(ftruncate(fd, BUFSIZE) != -1){
            if((buffer = mmap(NULL, BUFSIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) != ((void*)-1)){
                while(1){
                    puts("Banca: non sto facendo niente...");
                    sleep(rand() % 10);
                    sem_wait(cc);
                    puts("Banca: decido di operare sul CC");
                    int amount = (rand() % 100) - 50;
                    printf(amount < 0 ? "Banca: Preleverò " : "Banca: Depositerò ");
                    printf("un ammontare pari a %d\n", abs(amount));
                    *buffer += amount;
                    printf("Banca: il saldo corrente è ora di: %d\n", *buffer);
                    sem_post(cc);
                }
                cleanup(buffer);
                exit(EXIT_SUCCESS);
            }
        }
    }
    cleanup(buffer);
    exit(EXIT_FAILURE);
}
