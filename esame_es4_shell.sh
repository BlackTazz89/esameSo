#!/bin/bash

declare command;
echo "Eseguita la shell con pid: $$";
while (:)
do
    echo -n "%%";
    read -a line;
    declare -i n=0;
    while (( n < ${#line[@]} ))
    do
        declare token=${line[$n]}
        case $token in
            {)
                oldfd=$fd;
                exec {fd}< <($command 0<&${fd:-0};);
                if [[ ! -z $oldfd && $oldfd ]]; then eval exec "$oldfd"'<&-'; fi;
                command=;
                ;;
            '<--')
                n=$((n+1));
                exec {fd}<${line[$n]};
                ;;
            '-->')
                n=$((n+1));
                $command 0<&${fd:-0} 1>${line[$n]};
                command=
                ;;
            *)
                command="$command $token";
                ;;
        esac
        n=$n+1;
    done
    $command 0<&${fd:-0};
    if [[ ! -z $fd && $fd ]];then eval exec "$fd"'<&-'; fd=;fi;
    command=
done
