#define _POSIX_C_SOURCE 200809L
#define BUFSIZE sizeof(int)
#define N_USERS 10

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <time.h>

void user_loop(sem_t* cc, int* buffer){
    srand((unsigned) time(NULL));
    while(1){
        pid_t pid = getpid();
        printf("Cliente ID %d: non sto facendo niente\n", pid);
        sleep(rand() % 3);
        sem_wait(cc);
        printf("Cliente ID %d: decido di usare il CC\n", pid);
        int amount = (rand() % 100) - 50;
        printf(amount < 0 ? "Cliente ID %d: Preleverò " : "Cliente ID %d: Depositerò ", pid);
        printf("un ammontare pari a %d\n", abs(amount));
        *buffer += amount;
        printf("Cliente ID %d: il saldo corrente è ora di: %d\n", pid, *buffer);
        sem_post(cc);
    }
}

int main(int argc, char** argv){
    sem_t* cc;
    int* buffer;
    int fd;
    if((cc = sem_open("/es2_cc", O_RDWR)) == SEM_FAILED){
        perror("error while opening cc semaphore");
        exit(1);
    }
    if((fd = shm_open("/es2_buffer", O_RDWR, S_IRUSR | S_IWUSR)) != -1){
        if((buffer = mmap(NULL, BUFSIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) != ((void*)-1)){
            pid_t pid;
            for(int i = 0; i < N_USERS; i++){
                sleep(1);
                switch(pid = fork()){
                    case -1:
                        perror("Error while spawning child");
                        exit(EXIT_FAILURE);
                    case 0:
                        user_loop(cc, buffer);
                        exit(EXIT_SUCCESS);
                    default:
                        printf("Nuovo cliente con ID: %d\n", pid);
                }
            }
            for(int i = 0; i < N_USERS; i++){
                wait(NULL);
            }
            exit(EXIT_SUCCESS);
        }
    }
    perror("error while accessing shared memory");
    exit(EXIT_FAILURE);
}
